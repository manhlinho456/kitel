import java.util.Scanner;

public class Tens {
    public static void main(String[] args){
        int a;
        Scanner sc = new Scanner(System.in);
        a = sc.nextInt();
        if(a<1000 || a>9999) {
            System.out.println("Wrong number");
            return;
        }
        System.out.println("Chữ số hàng chục của a là " + (a%100)/10);
    }
}
